package com.epam.aircompany.dao;

import com.epam.aircompany.bean.Airplane;

/**
 * The Interface IAirplaneDao describes own DAO methods for entity Airplane.
 *
 * @author Dzmitry Hrushetski
 */
public interface IAirplaneDao extends IBaseDao<Airplane> {

}
