package com.epam.aircompany.dao.factory;

/**
 * Enum DAOFactoryType contains the list of databases or data sources
 * used by the application.
 * 
 * @author Dzmitry Hrushetski
 *
 */
public enum DaoFactoryType {
	MYSQL;
}
