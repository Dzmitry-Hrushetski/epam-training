package com.epam.aircompany.dao;

import com.epam.aircompany.bean.Airport;

/**
 * The Interface IAirportDao describes own DAO methods for entity Airport.
 * 
 * @author Dzmitry Hrushetski
 *
 */
public interface IAirportDao extends IBaseDao<Airport> {

}
